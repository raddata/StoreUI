package ir.radata.storeui;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener,
Fr1.OnFragmentInteractionListener,
Fr2.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View v) {
        Fragment f1=new Fr1();
        Fragment f2=new Fr2();
        FragmentManager fragmentManager=getSupportFragmentManager();
         // R.id.container   AS FrameLayout
        switch (v.getId())
        {
            case R.id.b1:
            {
                fragmentManager.beginTransaction().replace(R.id.container,f1).commit();
                break;
            }
            case R.id.b2:
            {
                fragmentManager.beginTransaction().replace(R.id.container,f2).commit();
                break;
            }
        }


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
